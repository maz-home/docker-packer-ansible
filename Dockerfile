FROM alpine:latest

LABEL maintainer="mickael.azema@gmail.com"

RUN apk add --no-cache git curl unzip \
    && \
    apk add --no-cache ansible \
    && \
    curl -sL -o /packer.zip  https://releases.hashicorp.com/packer/1.4.4/packer_1.4.4_linux_amd64.zip \
    && \
    unzip packer.zip && mv packer /usr/local/bin/\
    && \
    rm -f /packer.zip \
    && \
    chmod +x /usr/local/bin/packer \
    && \
    echo "===> Adding hosts for convenience..."  && \
    mkdir -p /etc/ansible                        && \
    echo 'localhost' > /etc/ansible/hosts